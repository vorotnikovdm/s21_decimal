#include "s21_decimal.h"

int s21_from_int_to_decimal(int src, s21_decimal *dst) {
  int error = 0;
  for (int i = 0; i < 4; i++) dst->bits[i] = 0;
  if (src == 0)
    dst->bits[0] = 0;
  else if ((src >= MAX_DECIMAL) || (src <= MIN_DECIMAL)) {
    error = 1;
  } else {
    if (src < 0) {
      src = -src;
      dst->bits[3] = MINUS;
    }
    int mask = 0xFFFFFFFF;
    dst->bits[0] = src & mask;
  }
  return error;
}

int s21_from_float_to_decimal(float src, s21_decimal *dst) {
  int error = 0, scale = 8, minus = 0;
  for (int i = 0; i < 4; i++) dst->bits[i] = 0;
  if ((fabs(src) < powl(10.0, -1 * 28)) & (fabs(src) > 0.0))
    error = 1;
  else if ((src >= MAX_DECIMAL) || (src <= MIN_DECIMAL))
    error = 1;
  else if ((src != src) || (isinf(src) == 1)) {
    error = 1;
  } else {
    if (src < 0) {
      src = -src;
      minus = 1;
    }
    unsigned int temp = (int)src;
    double tempDouble = src - temp;
    unsigned int tempInt = tempDouble * pow(10, 8);
    s21_decimal value_1 = {{temp, 0, 0, 0}};
    s21_decimal value_2 = {{tempInt, 0, 0, scale << 16}};
    s21_add(value_1, value_2, dst);
    if (minus == 1) dst->bits[3] |= MINUS;
  }
  return error;
}

int s21_from_decimal_to_int(s21_decimal src, int *dst) {
  int error = 0, exp = getExp(src.bits[3]), sign = src.bits[3] >> 31;
  if (exp != 0) {
    s21_decimal temp = {{0}};
    s21_truncate(src, &temp);
    if (((temp.bits[1] != 0) || (temp.bits[2] != 0)) ||
        ((sign == 0) & (temp.bits[0] > 2147483647)) ||
        ((sign == 1) & (temp.bits[0] > 2147483648)))
      error = 1;
    else {
      *dst = temp.bits[0];
      if (sign == 1) *dst = -*dst;
    }
  } else if (((src.bits[1] != 0) || (src.bits[2] != 0)) ||
             ((sign == 0) & (src.bits[0] > 2147483647)) ||
             ((sign == 1) & (src.bits[0] > 2147483648)))
    error = 1;
  else {
    *dst = src.bits[0];
    if (sign == 1) *dst = -*dst;
  }
  return error;
}

int s21_from_decimal_to_float(s21_decimal src, float *dst) {
  int error = 0, exp = getExp(src.bits[3]), sign = src.bits[3] >> 31;
  s21_decimal check = {{1, 0, 0, 28 << 16}}, zero = {{0, 0, 0, 0}};
  src.bits[3] = ((src.bits[3] << 1) >> 1);
  *dst = 0.0;
  if ((exp > 28) ||
      ((s21_is_less(src, check) == 1) & (s21_is_greater(src, zero)))) {
    error = 1;
    for (int i = 0; i < 4; i++) src.bits[i] = 0;
  } else {
    if (exp > 6) {
      src.bits[3] = ((exp - 6) << 16);
      s21_round(src, &src);
      src.bits[3] = 6 << 16;
      exp = 6;
    }
    s21_decimal temp = {{0}};
    s21_truncate(src, &temp);
    *dst = temp.bits[0];
    s21_decimal sub = {{0}};
    s21_sub(src, temp, &sub);
    *dst += sub.bits[0] / pow(10, exp);
  }
  if (sign == 1) *dst = -*dst;
  return error;
}
